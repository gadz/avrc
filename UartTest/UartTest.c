/*
 * UartTest.c
 *
 * Created: 06.08.2013 12:17:28
 *  Author: gad
 */ 


#define F_CPU 8000000L
#define LED1 		4
#define LED2		5
#define LED_PORT 	PORTD
#define LED_DDR		DDRD


#include <avr/io.h>
#include <util/delay.h>
#include <avr/interrupt.h>

ISR(USART_RXC_vect)
{
	switch(UDR)
	{
		case '1': LED_PORT |= 1<<LED2;		break;
		case '0': LED_PORT &= ~(1<<LED2);	break;
		
		case '3': LED_PORT |= 1<<LED1;		break;
		case '2': LED_PORT &= ~(1<<LED1);		break;
		
		default: break;
	}
}

int main(void)
{
	volatile unsigned char i;

	#define XTAL 8000000L
	#define baudrate 9600L
	#define bauddivider (XTAL/(16*baudrate)-1)
	#define HI(x) ((x)>>8)
	#define LO(x) ((x)& 0xFF)

	UBRRL = LO(bauddivider);
	UBRRH = HI(bauddivider);
	UCSRA = 0;
	UCSRB = 1<<RXEN|1<<TXEN|1<<RXCIE|0<<TXCIE;
	UCSRC = 1<<URSEL|1<<UCSZ0|1<<UCSZ1;


	LED_DDR = 1<<LED1|1<<LED2;
	sei();

	while(1)
	{
		i++;
/*		LED_PORT ^=1<<LED1;
			_delay_ms(1000);
	*/


	}

	return 0;
}
